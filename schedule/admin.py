from django.contrib import admin
from .models import PersonalSchedule

# Register your models here.
admin.site.register(PersonalSchedule)