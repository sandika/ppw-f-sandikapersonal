from django.shortcuts import render
from django.http import HttpResponseRedirect
from  .models import PersonalSchedule

from .forms import ScheduleForm

# Create your views here.

def schedule(request):
	form = ScheduleForm()
	my_data = PersonalSchedule.objects.all()

	return render(request, 'schedule.html', {'form' : form, 'my_data' : my_data})


def submit_form(request):
	if request.method == 'POST':
		form = ScheduleForm(request.POST)
		if form.is_valid():
			new_form = form.save()
			return HttpResponseRedirect('/schedule/')

	return render(request, 'schedule.html', {'form' : form})

def clear_data(request):
	PersonalSchedule.objects.all().delete()

	return HttpResponseRedirect('/schedule/')