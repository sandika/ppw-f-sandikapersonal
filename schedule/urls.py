from django.contrib import admin
from django.urls import path, include, re_path
from .views import *

urlpatterns = [
    path('', schedule),
    path('submitted-form/', submit_form),
    path('clear-data/', clear_data)
]