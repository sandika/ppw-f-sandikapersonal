from django import forms
from .models import PersonalSchedule

class ScheduleForm(forms.ModelForm):
	class Meta:
		model = PersonalSchedule
		fields = ['activity_name', 'date', 'location', 'category']
	activity_name = forms.CharField(label='Activity Name', max_length=30)
	date = forms.DateField(label="Date", widget=forms.SelectDateWidget(empty_label=("Year", "Month", "Day")))
	time = forms.TimeField(label="Time", widget=forms.TimeInput(format="%H:%M"))
	location = forms.CharField(label='Location', max_length=30)

	ACADEMIC = 'AC'
	EXTRACURRICULAR = 'EX'
	HOLIDAY = 'HL'
	PERSONAL = 'PS'
	CATEGORY_CHOICES = (
		(ACADEMIC, 'Academic'),
		(EXTRACURRICULAR, 'Extracurricular'),
		(HOLIDAY, 'Holiday'),
		(PERSONAL, 'Personal'),
		)	
	category = forms.ChoiceField(
		choices = CATEGORY_CHOICES,
		)