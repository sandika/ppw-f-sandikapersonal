from django.db import models
import django

# Create your models here.
class PersonalSchedule(models.Model):
	def __str__(self):
		return ('Activity: ' + self.activity_name)

	activity_name = models.CharField(max_length=30)
	date = models.DateField(default=django.utils.timezone.now)
	time = models.TimeField(default=django.utils.timezone.now)
	location = models.CharField(max_length=30)

	ACADEMIC = 'AC'
	EXTRACURRICULAR = 'EX'
	HOLIDAY = 'HL'
	PERSONAL = 'PS'
	CATEGORY_CHOICES = (
		(ACADEMIC, 'Academic'),
		(EXTRACURRICULAR, 'Extracurricular'),
		(HOLIDAY, 'Holiday'),
		(PERSONAL, 'Personal'),
		)

	category = models.CharField(
		max_length = 2,
		choices = CATEGORY_CHOICES,
		)
